# notes

Dirt simple notes app to help me learn flutter

# TODO:

 - [X] Screen that can display a dynamic list of notes (titles and content)
 - [X] Be able to add new notes and edit existing notes
 - [X] Save notes in a permanent database to you can close the app without losing them
 - [ ] Be able to delete notes