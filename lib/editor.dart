//import 'dart:io';

import 'package:flutter/material.dart';
import 'note.dart';

class NoteModify extends StatefulWidget {

  final String title;
  final String content;
  final void Function(Note note, NoteState noteState, {bool deleted}) noteStateChangeHandler;

  NoteModify({Key key, this.title, this.content, this.noteStateChangeHandler}) : super(key: key);

  @override
  State createState() => new NoteModifyState();
}

class NoteModifyState extends State<NoteModify> {

  final TextEditingController _titleTextController = new TextEditingController();
  final TextEditingController _contentTextController = new TextEditingController();

  FocusNode _contentFocusNode;

  @override
  void initState() {
    super.initState();

    print(widget.title);

    _titleTextController.text = widget.title;
    _contentTextController.text = widget.content;

    _contentFocusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        title: new Text('Create A Note'),
        backgroundColor: Colors.yellow,
        actions: <Widget>[
          new Container (
            margin: const EdgeInsets.symmetric(horizontal: 8.0),
            child: new IconButton(
              icon: new Icon(Icons.done),
              color: Colors.white70,
              onPressed: () => _finishCreateNote(context),
            ),
          )
        ],
      ),
      body: _buildNoteEditor(context),
      );
  }

  Widget _buildNoteEditor(BuildContext context) {
    return new Container(
      margin: const EdgeInsets.all(8.0),
      child: new Column(
        children: <Widget>[
          new TextField(
            controller: _titleTextController,
            onSubmitted: (String val) => FocusScope.of(context).requestFocus(_contentFocusNode), //Focus the content text box
            autofocus: true,
            style: TextStyle(fontSize: 18.0,),
            textCapitalization: TextCapitalization.sentences,
            decoration: new InputDecoration(
              hintText: "Test Scores",),
          ),
          new Flexible(
            child: new TextField(
              controller: _contentTextController,
              onSubmitted: null,
              expands: true,
              minLines: null,
              maxLines: null,
              textCapitalization: TextCapitalization.sentences,
              focusNode: _contentFocusNode,
              decoration: new InputDecoration(
                border: InputBorder.none,
                hintText: "Maths: A*\nBiology: D\nArt: B",),
            ),
          ),
        ],
      )
    );
  }

  void _finishCreateNote(BuildContext context) {
    String title = _titleTextController.text;
    String content = _contentTextController.text;

    int nullCount = 0;

    if (title == "") {
      title = " ";
      nullCount += 1;
    }
    if (content == "") {
      content = " ";
      nullCount += 1;
    }

    print(nullCount);
    
    Navigator.pop(context, nullCount != 2 ? new Note(id: generateId(title, content), title: title, content: content, onStateChange: widget.noteStateChangeHandler) : null);
  }
}