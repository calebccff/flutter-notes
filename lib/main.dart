//import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'note.dart';
import 'editor.dart';

Future<Database> database;

void main() async {
  String dbCreateStatement = "CREATE TABLE notes(id TEXT PRIMARY KEY, title TEXT, content TEXT, date TEXT)";
  database = openDatabase(
    join(await getDatabasesPath(), 'saved_notes.db'),
    onCreate: (db, version) {
      // Run the CREATE TABLE statement on the database.
      return db.execute(
        dbCreateStatement,
      );
    },
    onUpgrade: (db, oldVer, newVer) {
      print("Database changed from version $oldVer to $newVer");
      db.execute("DROP TABLE notes");
      return db.execute(dbCreateStatement);
    },
    version: 4,
  );

  runApp(new KalNotesApp());
}

class KalNotesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp (
      title: 'Caleb\'s Note App',
      home: new NotesPreviewScreen(),
    );
  }
}

class NotesPreviewScreen extends StatefulWidget {
  @override
  State createState() => new NotesPreviewState();
}

class NotesPreviewState extends State<NotesPreviewScreen> {
  List<Note> _notes = <Note>[];

  
  @override
  void initState() {
    super.initState();

    _loadNotes().then((success) => this.setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    print("Rebuilding notes preview, _notes now has length ${_notes.length}");
    return new Scaffold (
      backgroundColor: Colors.white,
      appBar: new AppBar(
        title: new Text('Caleb\'s Note App'),
        backgroundColor: Colors.blue,
        actions: <Widget>[
          new Container (
            margin: const EdgeInsets.symmetric(horizontal: 8.0),
            child: new IconButton(
              icon: new Icon(Icons.add),
              color: Colors.white70,
              onPressed: () => _createNote(context),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() { //Clear the notes
            _notes = <Note>[];
          });
        },
        child: Icon(Icons.clear),
        backgroundColor: Colors.yellow,
      ),
      body: new Column (
        children: <Widget>[
          new Expanded (
            child: new ListView.builder(
              padding: new EdgeInsets.symmetric(vertical: 8.0),
              itemBuilder: (_, int index) => _notes[index],
              itemCount: _notes.length,
              //reverse: true,
            )
          )
        ],
      )
    );
  }

  void _createNote(BuildContext context) async {
    final newNote = await Navigator.push(context, new MaterialPageRoute(builder: (context) => new NoteModify(
      noteStateChangeHandler: _noteStateChange,
      )
      ));
    _notes.add(newNote);
  }

  // _saveNotes() async {
  //   final Database db = await database;
  //   for (Note note in _notes) {
  //     await db.insert('notes', note.toMap());
  //   }
  // }

  _loadNotes() async {
    final Database db = await database;

    final List<Map<String, dynamic>> maps = await db.query('notes');

    print("Loading notes from db, ${maps.length} notes, first title is ${maps ?? maps[0]['title']}");

    _notes = List.generate(maps.length, (i) {
      return Note(
        title: maps[i]['title'],
        content: maps[i]['content'],
        id: maps[i]['id'],
        onStateChange: _noteStateChange,
      );
    });
  }

  _deleteNote(Note note) async {
    print("Removing note with id: ${note.id}");
    setState(() {
      _notes.removeAt(_notes.indexOf(note));
    });

    final Database db = await database;
    db.delete('notes', where: "id = ${note.id}");
  }

  _noteStateChange(Note note, NoteState noteState, {bool deleted = false}) async {
    Note _updatedNote;
    var _noteIndex = _notes.indexWhere((lNote) => lNote.id.compareTo(noteState.id) == 0);
    print("Removing note with index $_noteIndex, there are ${_notes.length} notes");
    _notes.removeAt(_noteIndex);
    if (!deleted) {
      _updatedNote = new Note(id: noteState.id, title: noteState.title, content: noteState.content, onStateChange: _noteStateChange,);
      _notes.insert(_noteIndex, _updatedNote);
    }
    setState(() {
      
    });

    //print("Created new note: ${_updatedNote.title}, deleted: $deleted");
    //print(_updatedNote.toMap());

    final Database db = await database;
    if (deleted)
      db.delete('notes', where: "id = \"${note.id}\"");
    else
      db.update('notes', _updatedNote.toMap()); //Use the new note as it has the right id
  }
}

