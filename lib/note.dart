//import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';

import 'editor.dart';

class Note extends StatefulWidget {

  final String title;
  final String content;
  final String id;
  final void Function(Note note, NoteState noteState, {bool deleted}) onStateChange;

  Note({Key key, this.title, this.content, this.id, this.onStateChange}) : super(key: key);

  @override
  State createState() {
    return new NoteState();
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'content': this.content
      };
  }
}

class NoteState extends State<Note> {

  String title;
  String content;
  String id;

  @override
  void initState() {
    super.initState();

    this.title   = widget.title;
    this.content = widget.content;
    this.id      = widget.id ?? generateIdState(this);
  }

  @override
  Widget build(BuildContext context) {
    return new Container (
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      decoration: new BoxDecoration (
        color: Colors.white70,
      ),
      child: new Column(
        children: <Widget>[
          new Divider(
            height: 4.0,
            color: Colors.transparent,
          ),
          new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Expanded(
              child: new Column (
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                    new Text(this.title, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),),
                    new Text(this.content, style: TextStyle(fontWeight: FontWeight.w400, color: Colors.black54), ),
                ],
              ),
            ),
            new Container (
              //margin: const EdgeInsets.symmetric(horizontal: 2.0),
              child: IconButton(
                icon: new Icon(Icons.delete),
                color: Colors.blueGrey,
                onPressed: () {
                  widget.onStateChange(widget, this, deleted: true); //Updates the notes id so delete works
                }
              ),
            ),
            new Container (
              //margin: const EdgeInsets.symmetric(horizontal: 2.0),
              child: IconButton(
                icon: new Icon(Icons.edit),
                color: Colors.blueGrey,
                onPressed: () => _editNote(context),
              ),
            ),
          ],
        ),
        new Divider(height: 2.0, color: Colors.black38,),
      ],
      )
        
    );
  }

  _editNote(BuildContext context) async {
    final modifiedNote = await Navigator.push(context, new MaterialPageRoute(builder: (context) => new NoteModify(title: widget.title, content: widget.content,)));
    if (modifiedNote != null) {
      setState(() {
        this.title = modifiedNote.title;
        this.content = modifiedNote.content;
      });
      widget.onStateChange(widget, this);
    }
  }
}

String generateId(String title, String content) {
  return (title.substring(0, title.length > 5 ? 5: title.length) + 
  content.substring(0, content.length > 5 ? 5: content.length) + 
  Random.secure().nextInt(1000000).toString()).replaceAll(" ", "");
}

String generateIdState(NoteState ns) {
  return (ns.title.substring(0, ns.title.length > 5 ? 5: ns.title.length) + 
  ns.content.substring(0, ns.content.length > 5 ? 5: ns.content.length) + 
  Random.secure().nextInt(1000000).toString()).replaceAll(" ", "");
}